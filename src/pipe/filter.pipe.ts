import { Pipe, PipeTransform } from '@angular/core';
import { CountryDataType } from '../interface/country-data-type'

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(data: CountryDataType[], inputData: string): any {
    return data.filter((e:any)=>{
      if(e.country.search(new RegExp(inputData,"i"))>-1)
      {
          return e
      }
      })
  }

}
