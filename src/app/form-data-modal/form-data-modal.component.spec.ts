import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDataModalComponent } from './form-data-modal.component';

describe('FormDataModalComponent', () => {
  let component: FormDataModalComponent;
  let fixture: ComponentFixture<FormDataModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormDataModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDataModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
