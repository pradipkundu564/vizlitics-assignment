import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalData } from '../../interface/modal-data';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-form-data-modal',
  templateUrl: './form-data-modal.component.html',
  styleUrls: ['./form-data-modal.component.scss']
})
export class FormDataModalComponent implements OnInit {

  dataKey: any[];
  formDataLib: any = {
    name: 'Name',
    address: 'Address',
    city: 'City',
    state: 'State',
    country: 'Country',
    number: 'Phone number',
    gender: 'Gender',
  }

  faTimesCircle = faTimesCircle;

  constructor(
    public dialogRef: MatDialogRef<FormDataModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }


  ngOnInit(): void {
    this.dataKey = Object.keys(this.data);
  }

  close() {
    this.dialogRef.close();
  }

}
