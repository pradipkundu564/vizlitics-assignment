import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CountryDataType } from '../interface/country-data-type'

@Injectable({
  providedIn: 'root'
})



export class AppService {


  constructor(private http: HttpClient) { }


  getCountries(url: string) {
    return this.http.get<CountryDataType[]>(url);
  }
}
