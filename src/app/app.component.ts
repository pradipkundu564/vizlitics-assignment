import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { CountryDataType } from '../interface/country-data-type';
import { ModalData } from '../interface/modal-data';
import { FilterPipe } from '../pipe/filter.pipe';
import { faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatDialog } from '@angular/material/dialog';
import { FormDataModalComponent } from './form-data-modal/form-data-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  allData: CountryDataType[];
  countryData: CountryDataType[];
  searchInput: string;
  page = 1;
  pageSize = 5;
  collectionSize: number;
  sorted: boolean = false;
  sortBy: string;
  sortingType: string = 'asc';
  selectedCountry: string = "";
  countryIndex: Number;
  emptyError: string = "This field can not be  empty!";
  previousPage: Number;

  dataForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    number: new FormControl('', [Validators.required, Validators.pattern("[0-9 ]{10}")]),
    address: new FormControl('', [Validators.required]),
    country: new FormControl('', [Validators.required]),
    state: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    gender: new FormControl('', [Validators.required]),
  });
  submitted: boolean;
  loading: boolean = true;
  checked: boolean = false;
  title: any;

  constructor(
    private appService: AppService,
    private filter: FilterPipe,
    public dialog: MatDialog
  ) {

  }
  faArrowUp = faArrowUp;
  faArrowDown = faArrowDown;
  ngOnInit(): void {
    let url: string = "http://universities.hipolabs.com/search?country";
    this.appService.getCountries(url).subscribe((countryData: CountryDataType[]) => {
      this.allData = countryData;
      this.countryData = this.allData;
      this.collectionSize = this.countryData.length;
      this.loading = false;
    })
  }


  search(event: Event): void {
    this.countryData = this.filter.transform(this.allData, this.searchInput);
    this.collectionSize = this.countryData.length;
    this.sorted && this.sort(this.sortBy, this.sortingType);
    this.page = 1;
  }

  sortData(type: string) {
    this.sorted = true;
    this.sortBy = type;
    this.sortingType = this.sortingType == 'asc' ? 'dsc' : 'asc';
    this.sort(type, this.sortingType);
  }

  sort(type: string, sortingType: string) {
    this.countryData = this.countryData.sort((a: any, b: any) => {
      let first = a[type].toLowerCase();
      let second = b[type].toLowerCase();

      if (first < second) {
        return sortingType == 'dsc' ? -1 : 1;
      }
      if (first > second) {
        return sortingType == 'dsc' ? 1 : -1;
      }
      return 0;
    })
  }

  selectCountry(countryName: string, i: Number, event: Event) {
    this.previousPage = this.page;
    if(i != this.countryIndex) {
      this.checked = true;
      this.selectedCountry = countryName;
      this.countryIndex = i;
      this.dataForm.controls.country.setValue(countryName);
    } else {
      this.checked = !this.checked;
      if(this.checked) {
        this.selectedCountry = countryName;
        this.countryIndex = i;
        this.dataForm.controls.country.setValue(countryName);
      } else {
        this.selectedCountry = '';
        this.countryIndex = -1;
        this.dataForm.controls.country.setValue('');
      }
    }
  }


  submit() {
    let modalData: ModalData = {
      name: this.dataForm.controls.name.value,
      address: this.dataForm.controls.address.value,
      city: this.dataForm.controls.city.value,
      state: this.dataForm.controls.state.value,
      country: this.dataForm.controls.country.value,
      number: this.dataForm.controls.number.value,
      gender: this.dataForm.controls.gender.value,
    }
    
    !this.dataForm.invalid && this.openDialog(modalData);
  }

  onFocusOutEvent(event: Event) {
    // console.log(event, this.dataForm)
  }

  openDialog(modalData: ModalData): void {
    const dialogRef = this.dialog.open(FormDataModalComponent, {
      data: modalData,
      height: '480px',
      width: '480px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
