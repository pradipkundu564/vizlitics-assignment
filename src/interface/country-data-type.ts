export interface CountryDataType {
    country: string,
    domains: string[],
    name: string,
    'state-province': any,
    web_pages: string[],
    alpha_two_code: string
}
