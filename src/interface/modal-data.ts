export interface ModalData {
    address: string;
    city: string;
    country: string;
    gender: string;
    name: string;
    number: string;
    state: string;
  }
